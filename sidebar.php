<?php
include ('config.php');
$result=$pdo->query("SELECT * FROM `users`");
$row=$result->fetch();

if($_POST['exit'] == 'ok'){
    unset($_SESSION);
    header("location:http://schooling/reg.php");
};
?>

<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <?php
                $image=$_SESSION['image'];
                echo '<img src="upload/avatar/'.$image.'" class="img-circle elevation-2" alt="User Image">';
                ?>
            </div>
            <div class="info">
                <?php
                echo '<a href="profile.php">'.$_SESSION['name'].' '.$_SESSION['surname'].'</a>'
                ?>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2 sidebar-mt">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->

                <li>
                    <a href="index.php" class="nav-link">Учебники</a>
                </li>
                <li >
                    <a href="video.php" class="nav-link">Видео</a>
                </li>
                <li>
                    <a href="#" class="nav-link">Аудио</a>
                </li>
                <li>
                    <a href="#" class="nav-link">Доп. материалы</a>
                </li>
            </ul>
        </nav>
        <nav class="mt-2 sidebar-mt">
            <ul class="nav nav-pills nav-sidebar flex-column">
                <?php
                if($_SESSION['role'] == 'admin') {
                    echo '<li >';
                    echo '<a href = "table_user.php" class="nav-link" > Список пользователей </a >';
                echo '</li >';
                echo '<li >';
                    echo '<a href = "add_user.php" class="nav-link" > Добавить Ученика </a >';
                echo '</li >';
                echo '<li >';
                    echo '<a href = "add_books.php" class="nav-link" > Добавить Учебник </a >';
                echo '</li >';
                } ?>
            </ul>
        </nav>
        <form action="" method="POST">
            <button type="submit" value="ok" name="exit" class="btn btn-info" style="position: relative; left: 15px;">Выход</button>
        </form>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
